# rediparse from https://gitlab.com/tjstum/rediparse/
# Copyright (c) 2018, Tim Stumbaugh
# This project is licensed under the BSD 3-clause "New" or "Revised" License.
from io import BytesIO
from typing import Union, Sequence
from collections import deque
from contextlib import suppress

_NOTHING = object()
CRLF = b"\r\n"
CRLF_LEN = len(CRLF)


class ReplyError(Exception):
    pass


class ProtocolError(Exception):
    pass


PossiblyDecoded = Union[str, bytes]
RedisTypes = Union[PossiblyDecoded, int, ReplyError, None]
Output = Union[RedisTypes, Sequence[RedisTypes], bool]


__version__ = "1.0.1"


class PythonReader:
    def __init__(
        self, *, encoding=_NOTHING, protocolError=ProtocolError, replyError=ReplyError
    ):
        self.buffer = BytesIO()
        if encoding is _NOTHING:
            encoding = "utf-8"
        self._check_error(protocolError)
        self.protocolError = protocolError
        self._check_error(replyError)
        self.replyError = replyError
        self.encoding = encoding
        self._parsed = deque()

    @staticmethod
    def _check_error(error_class):
        try:
            raise error_class("test")
        except TypeError:
            raise
        except Exception:
            pass

    def feed(self, incoming: bytes):
        self.buffer.write(incoming)

    def gets(self) -> Output:
        self._parse_buffer()
        try:
            return self._parsed.popleft()
        except IndexError:
            return False

    def _convert_output(self, outgoing: bytes) -> PossiblyDecoded:
        if self.encoding is not None:
            with suppress(UnicodeDecodeError):
                return outgoing.decode(self.encoding)
        return outgoing

    def _parse_simple_string(self, buffer: bytes, start: int, conversion=None):
        end = buffer.find(CRLF, start)
        if end == -1:
            return
        parsed = self._convert_output(buffer[start + 1 : end])
        if conversion:
            parsed = conversion(parsed)
        return parsed, end + CRLF_LEN

    def _parse_bulk_string(self, buffer: bytes, start: int):
        size_info = self._parse_simple_string(buffer, start, int)
        if size_info is None:
            return
        size, start_offset = size_info
        if size == -1:
            # This is a special case of a null value (None in Python)
            converted = None
            end_offset = start_offset
        else:
            end_offset = start_offset + size
            # In order to have a complete string, we need the length to be at least as large as
            # end_offset + the trailing CRLF
            if len(buffer) < end_offset + 1:
                return
            converted = self._convert_output(buffer[start_offset:end_offset])
            crlf_trailer = buffer[end_offset : end_offset + CRLF_LEN]
            if crlf_trailer != CRLF:
                raise self.protocolError("Bulk-String did not end with CRLF")
            end_offset += CRLF_LEN
        return converted, end_offset

    def _parse_array(self, buffer: bytes, start: int):
        array_size_info = self._parse_simple_string(buffer, start, int)
        if array_size_info is None:
            return
        array_size, offset = array_size_info
        if array_size == -1:
            # Special case of a null array
            return None, offset
        # All we have at this point is how many elements in the array, so there's no ability to
        # check the buffer length, so just keep going until we have either come up with all the
        # entries in the array or parsing a single one of them exhausted the buffer
        output = []
        for _ in range(array_size):
            parsed = self._parse_one(buffer, offset)
            if parsed is None:
                return
            value, offset = parsed
            output.append(value)
        return output, offset

    def _parse_one(self, buffer: bytes, start: int):
        prefix = buffer[start : start + 1]
        if prefix == b"":
            return None  # empty buffer
        if prefix == b"+":  # redis simple string
            parsed = self._parse_simple_string(buffer, start)
        elif prefix == b"-":  # redis error (simple string)
            parsed = self._parse_simple_string(buffer, start, self.replyError)
        elif prefix == b":":  # redis integer (simple string)
            parsed = self._parse_simple_string(buffer, start, int)
        elif prefix == b"$":  # redis bulk string
            parsed = self._parse_bulk_string(buffer, start)
        elif prefix == b"*":  # redis array
            parsed = self._parse_array(buffer, start)
        else:
            raise self.protocolError(
                "Can't decode prefix {!r}! Possible desync".format(prefix)
            )

        return parsed

    def _parse_buffer(self):
        value = self.buffer.getvalue()
        start = 0
        while True:
            parsed = self._parse_one(value, start)
            if parsed is None:
                self.buffer.seek(0)
                self.buffer.truncate()
                self.buffer.write(value[start:])
                break
            else:
                converted, start = parsed
                self._parsed.append(converted)


def BestReader(*args, **kwargs):
    try:
        from hiredis import Reader
    except (ImportError, NameError):
        return PythonReader(*args, **kwargs)
    else:
        return Reader(*args, **kwargs)  # pragma: no cover
