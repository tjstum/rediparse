"""
The hiredis-py tests, but using the PythonReader

This should ensure API compatibility. The tests have been reimplemented in a more modern
style. There are a few divergences from hiredis.Reader documented inline
"""
# The contents of these tests were adapted from hiredis-py/test/reader.py. hiredis is licensed
# under BSD 3-clause
from rediparse import PythonReader, ProtocolError, ReplyError
import pytest


@pytest.fixture
def basic_reader():
    yield PythonReader()


def test_nothing(basic_reader):
    assert basic_reader.gets() is False


def test_error_when_feeding_non_string(basic_reader):
    with pytest.raises(TypeError, match="a bytes-like object is required, not 'int'"):
        basic_reader.feed(47)


def test_protocol_error(basic_reader):
    basic_reader.feed(b"x")
    with pytest.raises(
        ProtocolError, match="Can't decode prefix b'x'! Possible desync"
    ):
        basic_reader.gets()


def test_protocol_error_with_custom_class():
    reader = PythonReader(protocolError=RuntimeError)
    reader.feed(b"x")
    with pytest.raises(RuntimeError):
        reader.gets()


def test_protocol_error_with_custom_callable():
    class CustomException(Exception):
        pass

    reader = PythonReader(protocolError=lambda e: CustomException(e))
    reader.feed(b"x")
    with pytest.raises(CustomException):
        reader.gets()


def test_fail_with_wrong_protocol_error_class():
    with pytest.raises(TypeError):
        PythonReader(protocolError="wrong")


def test_error_string(basic_reader):
    basic_reader.feed(b"-error\r\n")
    error = basic_reader.gets()
    assert type(error) is ReplyError
    assert error.args == ("error",)


def test_error_string_with_custom_class():
    class CustomException(Exception):
        pass

    reader = PythonReader(replyError=CustomException)
    reader.feed(b"-error\r\n")
    error = reader.gets()
    assert type(error) is CustomException
    assert error.args == ("error",)


def test_error_string_with_custom_callable():
    class CustomException(Exception):
        pass

    reader = PythonReader(replyError=lambda e: CustomException(e))
    reader.feed(b"-error\r\n")
    error = reader.gets()
    assert type(error) is CustomException
    assert error.args == ("error",)


def test_fail_with_wrong_reply_error_class():
    with pytest.raises(TypeError):
        PythonReader(replyError="wrong")


def test_errors_in_nested_multi_bulk(basic_reader):
    basic_reader.feed(b"*2\r\n-err0\r\n-err1\r\n")
    for expected, actual in zip(("err0", "err1"), basic_reader.gets()):
        assert type(actual) is ReplyError
        assert actual.args == (expected,)


def test_integer(basic_reader):
    value = 2 ** 63 - 1  # largest 64-bit signed integer
    basic_reader.feed(b":%d\r\n" % value)
    assert basic_reader.gets() == value


def test_status_string(basic_reader):
    basic_reader.feed(b"+ok\r\n")
    assert basic_reader.gets() == "ok"


def test_empty_bulk_string(basic_reader):
    basic_reader.feed(b"$0\r\n\r\n")
    assert basic_reader.gets() == ""


def test_bulk_string(basic_reader):
    basic_reader.feed(b"$5\r\nhello\r\n")
    assert basic_reader.gets() == "hello"


def test_bulk_string_without_encoding():
    reader = PythonReader(encoding=None)
    snowman = b"\xe2\x98\x83"
    reader.feed(b"$3\r\n%b\r\n" % snowman)
    assert reader.gets() == snowman


def test_bulk_string_with_encoding():
    reader = PythonReader(encoding="utf-8")
    snowman = b"\xe2\x98\x83"
    reader.feed(b"$3\r\n%b\r\n" % snowman)
    assert reader.gets() == "\N{SNOWMAN}"


def test_bulk_string_with_other_encoding():
    reader = PythonReader(encoding="ascii")
    snowman = b"\xe2\x98\x83"
    reader.feed(b"$3\r\n%b\r\n" % snowman)
    assert reader.gets() == snowman


def test_bulk_string_with_invalid_encoding():
    reader = PythonReader(encoding="unknown")
    reader.feed(b"$5\r\nhello\r\n")
    with pytest.raises(LookupError):
        reader.gets()


def test_null_multi_bulk(basic_reader):
    basic_reader.feed(b"*-1\r\n")
    assert basic_reader.gets() is None


def test_multi_bulk(basic_reader):
    basic_reader.feed(b"*2\r\n$5\r\nhello\r\n$5\r\nworld\r\n")
    assert basic_reader.gets() == ["hello", "world"]


# Since an invalid encoding is basically fatal, I don't see any reason to duplicate
# hiredis' interface of waiting until the reply is complete before raising. This
# test, therefore, will never pass, but it is included here to be thorough
@pytest.mark.xfail(raises=LookupError, reason="Intentional API difference", strict=True)
def test_multi_bulk_with_invalid_encoding_and_partial_reply():
    reader = PythonReader(encoding="unknown")
    reader.feed(b"*2\r\n$5\r\nhello\r\n")
    assert reader.gets() is False
    reader.feed(b":1\r\n")
    with pytest.raises(LookupError):
        reader.gets()


def test_nested_multi_bulk(basic_reader):
    basic_reader.feed(b"*2\r\n*2\r\n$5\r\nhello\r\n$5\r\nworld\r\n$1\r\n!\r\n")
    assert basic_reader.gets() == [["hello", "world"], "!"]


def test_nested_multi_bulk_depth(basic_reader):
    basic_reader.feed(b"*1\r\n*1\r\n*1\r\n*1\r\n$1\r\n!\r\n")
    assert basic_reader.gets() == [[[["!"]]]]


def test_subclassable():
    class TestReader(PythonReader):
        def __init__(self):
            super().__init__()

    reader = TestReader()
    reader.feed(b"+ok\r\n")
    assert reader.gets() == "ok"


# PythonReader.feed does not take in offset or length parameters, so don't test that


def test_feed_bytearray(basic_reader):
    basic_reader.feed(bytearray(b"+ok\r\n"))
    assert basic_reader.gets() == "ok"
