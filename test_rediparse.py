"""
Custom tests for PythonReader and rediparse in general

The tests for this package consist of this module in addition to test_like_hiredis.py
"""

from rediparse import PythonReader, ReplyError, BestReader, ProtocolError
import pytest


@pytest.fixture
def basic_reader():
    yield PythonReader()


@pytest.fixture
def no_hiredis(monkeypatch):
    hiredis = pytest.importorskip("hiredis")
    monkeypatch.delattr(hiredis, "Reader")


def test_incomplete(basic_reader):
    basic_reader.feed(b"+ok")
    assert basic_reader.gets() is False
    basic_reader.feed(b"\r\n")
    assert basic_reader.gets() == "ok"
    assert basic_reader.gets() is False


def test_incomplete_array(basic_reader):
    snowman = b"\xe2\x98\x83"
    full_data = b"*3\r\n$3\r\n%b\r\n+safe\r\n:5\r\n" % snowman
    basic_reader.feed(full_data[:8])
    assert basic_reader.gets() is False
    basic_reader.feed(full_data[8:16])
    assert basic_reader.gets() is False
    basic_reader.feed(full_data[16:])
    assert basic_reader.gets() == ["\N{SNOWMAN}", "safe", 5]


def test_multiple_replies(basic_reader):
    basic_reader.feed(b"+ok\r\n" * 4)
    assert basic_reader.gets() == "ok"
    assert basic_reader.gets() == "ok"
    assert basic_reader.gets() == "ok"
    assert basic_reader.gets() == "ok"
    assert basic_reader.gets() is False


def test_null_bulk_string(basic_reader):
    basic_reader.feed(b"$-1\r\n")
    assert basic_reader.gets() is None
    assert basic_reader.gets() is False


def test_null_elements_in_arrays(basic_reader):
    basic_reader.feed(b"*3\r\n$3\r\nfoo\r\n$-1\r\n$3\r\nbar\r\n")
    assert basic_reader.gets() == ["foo", None, "bar"]


def test_array_of_three_integers(basic_reader):
    basic_reader.feed(b"*3\r\n:1\r\n:2\r\n:3\r\n")
    assert basic_reader.gets() == [1, 2, 3]


def test_array_mixed_type(basic_reader):
    basic_reader.feed(b"*5\r\n:1\r\n:2\r\n:3\r\n:4\r\n$6\r\nfoobar\r\n")
    assert basic_reader.gets() == [1, 2, 3, 4, "foobar"]


def test_random_bytes(basic_reader):
    import random

    binary_data = bytearray(random.getrandbits(8) for _ in range(1000))
    basic_reader.feed(b"$1000\r\n%b\r\n" % binary_data)
    assert basic_reader.gets() == binary_data


def test_embedded_crlf(basic_reader):
    basic_reader.feed(b"*3\r\n$5\r\nth\r\n4\r\n:17\r\n-Bummer\r\n")
    output = basic_reader.gets()
    assert output[:2] == ["th\r\n4", 17]
    assert type(output[2]) is ReplyError
    assert output[2].args == ("Bummer",)


def test_disabled_encoding():
    reader = PythonReader(encoding=None)
    reader.feed(b"+ok\r\n")
    assert reader.gets() == b"ok"


def test_violating_bulk_string(basic_reader):
    basic_reader.feed(b"$2\r\nhi")
    assert basic_reader.gets() is False
    basic_reader.feed(b"\x00\x00")
    with pytest.raises(ProtocolError):
        basic_reader.gets()


# These tests may be slightly confusing, but the goal is to have tests that provide
# adequate coverage despite the presence or lack thereof of hiredis. See this handy chart:
# | hiredis | test_hiredis_fallback | test_hiredis_available | test_hiredis_not_installed |
# | ------- | --------------------- | ---------------------- | -------------------------- |
# | present |          run          |           run          |             skip           |
# | absent  |         skip          |          skip          |              run           |


def test_hiredis_fallback(no_hiredis):
    reader = BestReader()
    assert type(reader) == PythonReader


def test_hiredis_available():
    hiredis = pytest.importorskip("hiredis")
    reader = BestReader()
    assert type(reader) == hiredis.Reader


def test_hiredis_not_installed():
    try:
        import hiredis
    except ImportError:
        pass
    else:
        pytest.skip("Hiredis installed")
    reader = BestReader()
    assert type(reader) == PythonReader
