# rediparse

[![pipeline status](https://gitlab.com/tjstum/rediparse/badges/master/pipeline.svg)](https://gitlab.com/tjstum/rediparse/commits/master)
[![coverage report](https://gitlab.com/tjstum/rediparse/badges/master/coverage.svg)](https://gitlab.com/tjstum/rediparse/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)


Pure Python parser of [RESP](https://redis.io/topics/protocol) (the REdis Serialization Protocol).
This implementation (mostly) conforms to the same interface as what
[hiredis-py](https://github.com/redis/hiredis-py) provides, but can be used anywhere you have Python

## Install
TBD, but honestly, feel free to drop in `rediparse.py` to your project.
It's a single file, and the protocol is very stable.

## Requirements
rediparse requires Python 3.5 or later.

The tests are written with, and require, [py.test](pytest.org)

## Usage
The `rediparse` module contains the `PythonReader` class and the `BestReader` function. The
`BestReader` function can be used when you don't care particularly for which parser implementation:
it gives you `hiredis.Reader` if available and `rediparse.PythonReader` otherwise. Both
implementations have the same interface, and neither of them handle I/O.

The remaining usage description will use `PythonReader`, but the examples are equally as valid as
what `hiredis.Reader` provides, except where documented otherwise.

### Reply Parser
`PythonReader` contains two methods for parsing replies from streams of data. `PythonReader.feed`
takes in a byte string (`bytes`) object, which is appended to an internal buffer.
`PythonReader.gets` parses the buffer and returns a reply when the buffer contains a full reply.
```python
>>> reader = rediparse.PythonReader()
>>> reader.feed(b"$5\r\nhello\r\n")
>>> reader.gets()
'hello'
```

If there is no reply yet available (i.e. more bytes are needed), `gets` returns
`False`:
```python
>>> reader.feed("*2\r\n$5\r\nhello\r\n")
>>> reader.gets()
False
>>> reader.feed("$5\r\nworld\r\n")
>>> reader.gets()
['hello', 'world']
```

It's possible that multiple replies may be available at once, in which case, successive calls to
`gets` may be warranted:
```python
>>> reader.feed(b"+hello\r\n")
>>> reader.feed(b"+world\r\n")
>>> reader.gets()
'hello'
>>> reader.gets()
'world'
>>> reader.gets()
False
```

#### Text handling
The Redis protocol itself deals in bytes. By default, `PythonReader` will assume that those bytes
represent text encoded in UTF-8.
```python
>>> reader.feed(b"$3\r\n\xe2\x98\x83\r\n")
>>> reader.gets()
'☃'
```
**NOTE**: This differs from `hiredis.Reader`, which does not perform any decoding by default. To
change the default encoding, specify a different one (or `None` to disable automatic decoding):
```python
>>> reader = rediparse.PythonReader(encoding=None)
>>> reader.feed(b"$3\r\n\xe2\x98\x83\r\n")
>>> reader.gets()
b'\xe2\x98\x83'
```
If the specified encoding (or the default one) fails to decode the received bytes, the bytes will
be returned as-is, just as if the encoding was set to `None`:
```python
>>> reader = rediparse.PythonReader(encoding="utf-32")
>>> reader.feed(b"$3\r\n\xe2\x98\x83\r\n")
>>> reader.gets()
b'\xe2\x98\x83'
```
However, if the encoding specified does not even exist, a `LookupError` is raised after calling
`gets` (which is the same thing as what would happen if you called `decode` yourself).

Redis integers are returned as Python integers.

#### Error handling
When a protocol error occurs (some stream corruption that results in a loss of synchronization as
defined in the protocol), the error `rediparse.ProtocolError` is raised when calling `gets`.
There's very little that can be done at this point, so the connection should be discarded.

Redis can return errors as a reply (for example, if the command doesn't exist, or if the command
was executed against the wrong type of key). In this case, `rediparse.ReplyError` is *returned*,
not raised.

To change the class of exception that should be used for either of these cases, `PythonReader`
takes `replyError` and `protocolError` as keyword arguments.

## Differences from `hiredis`
The API of `PythonReader` conforms to that of `hiredis.Reader` with the following exceptions:

- By default, `PythonReader` will attempt to decode replies as UTF-8 encoded text. This just means
that the default of `PythonReader` is as if `encoding="utf-8"` was given.
    - To disable this, pass `encoding=None` to the initializer
    - This does not affect Redis integers, which are returned as `int`s
- When an invalid encoding is specified, a `LookupError` may be raised even if an entire reply
has not yet been received (via `feed`).
