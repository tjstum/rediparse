#!/usr/bin/env python3

from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="rediparse",
    version="1.0.1",
    description="Pure Python parser for the Redis protocol",
    long_description=long_description,
    url="https://gitlab.com/tjstum/rediparse",
    author="Tim Stumbaugh",
    author_email="me@tjstum.com",
    license="BSD 3-Clause",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Build Tools",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    keywords=["redis", "hiredis"],
    py_modules=["rediparse"],
    python_requires=">=3.5",
)
